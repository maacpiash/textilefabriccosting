﻿using System;
using System.Collections.Generic;

namespace TextileFabricCosting
{
    [Serializable]
    public class Costing
    {
        public static int TotalID { get; set; }

        public string Name { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public string Construction { get; set; }
        public double Width { get; set; }
        public DateTime CreatedOn { get; set; }

        public double WarpRate { get; set; }
        public double SizingRate { get; set; }
        public double WarpCost { get; set; }
        public double WeftRate { get; set; }
        public double WeftCost { get; set; }

        public double Dyeing { get; set; }
        public double Interest { get; set; }
        public double Staff { get; set; }
        public double Wastage { get; set; }
        public double Commission { get; set; }
        public double Profit { get; set; }

        public double FinalPrice { get; set; }

        public Costing(string name)
        {
            Name = name;
            CreatedOn = DateTime.Now;
            Id = ++TotalID;
        }

        public Costing(string name, string type, string construction, double width)
        {
            Name = name;
            Type = type;
            Construction = construction;
            Width = width;
            CreatedOn = DateTime.Now;
            Id = ++TotalID;
        }

        public override string ToString()
        {
            return string.Format("[FabricCosting: Name={0}, Id={1}, Type={2}, Construction={3}, Width={4}, Stamp={5}, WarpRate={6}, WarpCost={7}, WeftRate={8}, WeftCost={9}, Dyeing={10}, Interest={11}, Staff={12}, Wastage={13}, Commission={14}, Profit={15}, FinalPrice={16}]", Name, Id, Type, Construction, Width, CreatedOn, WarpRate, WarpCost, WeftRate, WeftCost, Dyeing, Interest, Staff, Wastage, Commission, Profit, FinalPrice);
        }


        // Singleton object for current session
        private static Costing currentInstance;
        private static readonly object locker = new object();
        public static Costing CurrentInstance
        {
            get
            {
                if (currentInstance == null)
                {
                    lock (locker)
                    {
                        if (currentInstance == null)
                            currentInstance = new Costing();
                    }
                }
                return currentInstance;
            }
        }
        private Costing() { CreatedOn = DateTime.Now; }

        public static Dictionary<string, List<string>> Dict()
        {
            Dictionary<string, List<string>> fabrics = new Dictionary<string, List<string>>();
            fabrics.Add("100% Cotton Sheeting",
                        Constructions(30, 30, 68, 68, 20, 20, 60, 60));
            fabrics.Add("35/65 T/C",
                        Constructions(45, 45, 110, 76, 45, 45, 100, 72));
            fabrics.Add("100% Cotton Voile", Constructions(60, 60, 110, 72));
            fabrics.Add("100% Cotton Herringbone Twill",
                        Constructions(30, 30, 133, 72, 30, 30, 96, 68, 20, 20, 112, 62));
            fabrics.Add("100% Cotton Yarn dyed Chambray",
                       Constructions(20, 20, 78, 52, 30, 30, 110, 74, 40, 40, 110, 88));
            fabrics.Add("100% Cotton Twill",
                        Constructions(7, 7, 72, 42, 10, 7, 72, 42, 10, 10, 72, 42, 16, 10, 104, 56, 20, 7, 108, 42, 16, 12, 104, 56, 20, 10, 108, 54, 20, 16, 128, 60, 20, 20, 108, 58, 20, 20, 133, 60, 30, 20, 120, 76, 30, 30, 133, 72, 40, 40, 130, 100));
            fabrics.Add("T/C Twill",
                        Constructions(16, 12, 104, 56, 20, 16, 128, 80, 20, 20, 108, 58));
            fabrics.Add("T/C Canvas", Constructions(20, 20, 100, 50));
            fabrics.Add("100% Cotton Canvas",
                        Constructions(20, 20, 100, 50, 20, 16, 100, 50, 20, 7, 72, 42, 16, 16, 93, 72, 16, 12, 108, 58, 20, 10, 72, 42, 10, 10, 72, 42));
            fabrics.Add("100% Cotton Oxford Canvas",
                        new List<string>() { "20+20×16/128×80", "30×30+30/116×70" });
            fabrics.Add("100% Cotton Panama Canvas",
                        new List<string>() { "16+16 × 12+12 / 128 × 80", "12+12 × 7+7 / 72 × 42" });
            fabrics.Add("100% Cotton Automan", new List<string>() { "16×10/2 / 122×46" });
            fabrics.Add("100% Cotton Calico", Constructions(16, 16, 60, 60));
            fabrics.Add("100% Cotton Poplin", Constructions(40, 40, 110, 70, 40, 40, 133, 72, 40, 40, 133, 100));
            return fabrics;
        }

        public static List<string> Constructions(params int[] numbers)
        {
            if (numbers.Length < 4 || numbers.Length % 4 != 0)
                throw new Exception("Length of parameters must be a positive multiple of 4.");
            List<string> constructions = new List<string>();
            for (int i = 0; i < numbers.Length; i += 4)
                constructions.Add(String.Format("{0}×{1}/{2}×{3}", numbers[i], numbers[i + 1], numbers[i + 2], numbers[i + 3]));
            return constructions;
        }

        public static string[] SplitConst(string text)
        {
            string[] construction = new string[4];
            for (int i = 0; i < 4; i++)
                construction[i] = text.Split('/')[i / 2].Split('×')[i % 2];
                // [i / 2] = [0], [0], [1], [1]
                // [i % 2] = [0], [1], [0], [1]
            return construction;
        }
    }
}
