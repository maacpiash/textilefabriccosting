﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace TextileFabricCosting
{
    [Activity(Label = "Costing : Warp")]
    public class CostingWarpActivity : Activity
    {
        string FabricType, FabricConst;
        TextView TypeLabel, ConstLabel;
        EditText WidthTxt, ThrDensTxt, MassPoundTxt, ThrPickTxt, UnitPriceTxt, UnitWarpTxt, TotalPriceTxt;
        Button button, NextButn;
        RadioButton RadioKG, RadioLB;

        const int POUND = 840;
        const double WASTAGE_WARP = 1.15;
        const int BOTH_SIDES = 20;
        double width;
        string[] construction;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.CostingWarpLayout);

            GetValues();
            FindViewsById();
            SetViewEventHandlers();
        }

        void GetValues()
        {
            try
            {
                FabricType = Costing.CurrentInstance.Type;
                FabricConst = Costing.CurrentInstance.Construction;
                width = Costing.CurrentInstance.Width;
            }
            catch
            {
                FabricType = "Type";
                FabricConst = "1×1/2×2";
                width = 0.0;
            }

            construction = Costing.SplitConst(FabricConst);
        }

        void FindViewsById()
        {
            // TextView
            TypeLabel = FindViewById<TextView>(Resource.Id.title);
            TypeLabel.Text = FabricType;
            ConstLabel = FindViewById<TextView>(Resource.Id.textView1);
            ConstLabel.Text += FabricConst;

            // Button
            button = FindViewById<Button>(Resource.Id.myButton);
            NextButn = FindViewById<Button>(Resource.Id.next);

            //EditText
            WidthTxt = FindViewById<EditText>(Resource.Id.FabricWidthInchText);
            WidthTxt.Text = width.ToString();
            ThrDensTxt = FindViewById<EditText>(Resource.Id.ThreadDensityText);
            MassPoundTxt = FindViewById<EditText>(Resource.Id.ThreadMassPoundsText);
            ThrPickTxt = FindViewById<EditText>(Resource.Id.ThreadPickText);
            UnitPriceTxt = FindViewById<EditText>(Resource.Id.ThreadPriceRateText);
            UnitWarpTxt = FindViewById<EditText>(Resource.Id.WarpingCostText);
            TotalPriceTxt = FindViewById<EditText>(Resource.Id.TotalPriceText);
            ThrPickTxt.Text = construction[1];
            ThrDensTxt.Text = construction[3];

            // Radio
            RadioKG = FindViewById<RadioButton>(Resource.Id.kgRadio);
            RadioLB = FindViewById<RadioButton>(Resource.Id.lbRadio);
        }

        void SetViewEventHandlers()
        {
            button.Click += (sender, e) => Calculate(RadioLB.Selected);
            RadioKG.Click += (sender, e) => Calculate(false);
            RadioLB.Click += (sender, e) => Calculate(true);

            NextButn.Click += delegate {
                try { width = Convert.ToDouble(WidthTxt.Text); }
                catch { width = 0.0; }
                Costing.CurrentInstance.Width = width;

                double rate;
                try { rate = Convert.ToDouble(UnitPriceTxt.Text); }
                catch { rate = 0.0; }
                Costing.CurrentInstance.WarpRate = rate;

                double cost;
                try { cost = Convert.ToDouble(TotalPriceTxt.Text); }
                catch { cost = 0.0; }
                Costing.CurrentInstance.WarpCost = cost;

                var activity = new Intent(this, typeof(CostingWeftActivity));
                StartActivity(activity);
            };
        }

        void Calculate(bool UnitIsPound)
        {
            double dens, pick, pound, unitPrice, total;
            try { width = Convert.ToDouble(WidthTxt.Text); }
            catch { width = 0.0; }
            double.TryParse(WidthTxt.Text, out width);
            double.TryParse(ThrDensTxt.Text, out dens);
            double.TryParse(ThrPickTxt.Text, out pick);
            pound = (width * dens + BOTH_SIDES) / (POUND * pick);
            pound *= WASTAGE_WARP;
            MassPoundTxt.Text = $"{(pound):0.000}";
            try
            {
                unitPrice = Convert.ToDouble(UnitPriceTxt.Text) + Convert.ToDouble(UnitWarpTxt.Text);
                if (!UnitIsPound) unitPrice /= 2.20462;
            }
            catch
            {
                unitPrice = 0;
            }

            total = pound * unitPrice;
            TotalPriceTxt.Text = $"{total:0.00}";
        }
    }
}
