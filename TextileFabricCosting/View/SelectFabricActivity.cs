﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TextileFabricCosting
{
    [Activity(Label = "Costing", MainLauncher = true, Icon = "@mipmap/icon")]
    public class SelectFabricActivity : Activity
    {
        Dictionary<string, List<string>> FabricsDict;
        Spinner FabricList, ConstList;
        Button ViewSavedButn, NextButn;
        EditText WidthTxt;
        string FabricType, FabricConst;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SelectFabricLayout);
            FabricsDict = Costing.Dict();
            FindViewsById();
            SetViewEventHandlers();
        }

        private void SetViewEventHandlers()
        {
            String[] array_spinner = FabricsDict.Keys.ToArray();
            var adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, array_spinner);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            FabricList.Adapter = adapter;
            FabricList.ItemSelected += FabricList_ItemSelected;
            NextButn.Click += delegate {
                var activity = new Intent(this, typeof(CostingWarpActivity));
                Costing.CurrentInstance.Type = FabricType;
                Costing.CurrentInstance.Construction = FabricConst;
                double w;
                double.TryParse(WidthTxt.Text, out w);
                Costing.CurrentInstance.Width = w;
                StartActivity(activity);
            };
        }

        private void FindViewsById()
        {
            FabricList = FindViewById<Spinner>(Resource.Id.fabricList);
            ConstList = FindViewById<Spinner>(Resource.Id.constList);
            ViewSavedButn = FindViewById<Button>(Resource.Id.viewSavdButn);
            WidthTxt = FindViewById<EditText>(Resource.Id.widthText);
            NextButn = FindViewById<Button>(Resource.Id.nextButn);
        }

        void FabricList_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            FabricType = (sender as Spinner).GetItemAtPosition(e.Position).ToString();
            UpdateSecondSpinner();
        }

        void UpdateSecondSpinner() // => ViewSavedButn.Text = FabricType;
        {
            String[] array_spinner = FabricsDict[FabricType].ToArray();
            var adapter = new ArrayAdapter(this,
                Android.Resource.Layout.SimpleSpinnerItem, array_spinner);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            ConstList.Adapter = adapter;
            ConstList.ItemSelected += (sender, e)
                => FabricConst = (sender as Spinner).GetItemAtPosition(e.Position).ToString();
        }

    }
}