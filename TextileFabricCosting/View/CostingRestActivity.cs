﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TextileFabricCosting
{
    [Activity(Label = "CostingRestActivity")]
    public class CostingRestActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.CostingRestLayout);

            GetValues();
            FindViewsById();
            SetViewEventHandlers();
            Calculate();
        }

        private void GetValues()
        {
            // TODO
        }

        private void FindViewsById()
        {
            // TODO
        }

        private void SetViewEventHandlers()
        {
            // TODO
        }

        private void Calculate()
        {
            // TODO
        }
    }
}
