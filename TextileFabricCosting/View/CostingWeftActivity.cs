﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TextileFabricCosting
{
    [Activity(Label = "Costing : Weft")]
    public class CostingWeftActivity : Activity
    {
        const double WASTAGE_WEFT = 1.02;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.CostingWeftLayout);

            GetValues();
            FindViewsById();
            SetViewEventHandlers();
            Calculate();
        }

        private void GetValues()
        {
            // TODO
        }

        private void FindViewsById()
        {
            // TODO
        }

        private void SetViewEventHandlers()
        {
            // TODO
        }

        private void Calculate()
        {
            // TODO
        }
    }
}
